#!/bin/bash
sed -i 's/,/;/g' $1
sed -i 's/ kph//g' $1
sed -i 's/ revolutions_per_minute//g' $1
sed -i 's/ percent//g' $1
sed -i 's/None/0.0/g' $1
