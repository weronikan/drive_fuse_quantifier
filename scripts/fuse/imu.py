import sys, getopt

sys.path.append('.')
import RTIMU
import os.path
import time
import math
import time

SETTINGS_FILE = "RTIMULib"  # wskazanie pliku z parametrami kalibrującymi (plik konfiguracyjny)


class SensorIMU:
    def __init__(self, logger, dataThreadLock):
        self.logger = logger
        self.dataThreadLock = dataThreadLock
        self.accelData = [0.0, 0.0, 0.0]
        self.gyroData = [0.0, 0.0, 0.0]

        self.logger.logEvent("[IMU] Using settings file " + SETTINGS_FILE + ".ini")
        if not os.path.exists(SETTINGS_FILE + ".ini"):  # wypis diagnostyczny
            self.logger.logEvent("[IMU] Settings file does not exist, will be created")

        self.s = RTIMU.Settings(SETTINGS_FILE)  # załadowanie pliku konfiguracyjnego
        self.imu = RTIMU.RTIMU(self.s)  # kalibracja urządzenia z użyciem wcześniej załadowanego pliku konfig.

        self.logger.logEvent("[IMU] IMU Name: " + self.imu.IMUName())  # wypis diagnostyczny

        if (not self.imu.IMUInit()):  # wypis diagnostyczny - weryfikacja nawiązania połączenia
            self.logger.logEvent("[IMU] IMU Init Failed")
            sys.exit(1)
        else:
            self.logger.logEvent("[IMU] IMU Init Succeeded")

        self.logger.logEvent("[IMU] enabling IMU")
        self.imu.setSlerpPower(0.02)  # parametryzacja urządzenia. Zgodnie z treścią biblioteki uruchamiamy
        self.imu.setGyroEnable(True)  # żyroskop, kompas i akcelerometr na potrzeby naszych testów.
        self.imu.setAccelEnable(True)
        self.imu.setCompassEnable(True)
        self.poll_interval = self.imu.IMUGetPollInterval()  # wypis diagnostyczny
        self.logger.logEvent("[IMU] Recommended Poll Interval: %dmS" % self.poll_interval)

    def read(self):
        self.imu.setAccelEnable(True)
        access = self.imu.IMURead()
        self.logger.logEvent("[IMU] read %s."%access)
        if access:
            data = self.imu.getIMUData()  # pobranie danych o IMU
            self.dataThreadLock.acquire()
            self.accelData = self.imu.getAccel()  # pobranie danych z akcelerometru
            self.gyroData = self.imu.getGyro()  # pobranie danych z żyroskopu
            self.dataThreadLock.release()

    def getGyro(self):
        return self.gyroData

    def getAccel(self):
        return self.accelData

    def getInterval(self):
        return self.poll_interval
