import time  # dołączenie biblioteki czasu

import obd  # import biblioteki obd (ze wsparciem komunikacji OBD2)
from obd import OBDStatus  # (j.w.)


class SensorOBD:
    statusConnected = False
    connection = None
    engineLoad = 0.0
    rpm = 0.0
    speed = 0.0
    throttlePosition = 0.0

    def __init__(self, logger, dataThreadLock, port):
        self.logger = logger
        self.dataThreadLock = dataThreadLock
        self.connection = None
        self.engineLoad = 0.0
        self.rpm = 0.0
        self.speed = 0.0
        self.throttlePosition = 0.0
        self.port = port

    # (1) Funkcja sprawdza dostępne połączenia magistrali szeregowej i nawiązuje połączenie z dostępnym modułem ODB
    def scanAndConnect(self):
        self.logger.logEvent("[OBD] Scanning ports... Using %s"%self.port)

        # nawiązujemy połączenie z pierwszym dostępnym portem
        obdconnection = obd.OBD(self.port)

        # diagnostyczne wypisy z informacją o nawiązaniu połączenia i wynegocjowanym protokołem (ISO 14230-4)
        self.logger.logEvent("[OBD] %s" % obdconnection.is_connected())
        self.logger.logEvent("[OBD] %s " % obdconnection.status())
        self.logger.logEvent("[OBD] %s " % obdconnection.protocol_id())
        return obdconnection

    # (2) Funkcja weryfikująca poprawne nawiązanie połączenia z pojazdem
    def printStatus(self):
        # błąd braku połączenia
        if self.connection.status() == OBDStatus.NOT_CONNECTED:
            self.logger.logEvent("[OBD] %s " % "Not connected")
            return False

        # weryfikacja wstępnego połączenia z modułem ELM
        if self.connection.status() == OBDStatus.ELM_CONNECTED:
            self.logger.logEvent("[OBD] %s " % "ELM connected")
        else:
            self.logger.logEvent("[OBD] %s " % "ELM not connected")

        # weryfikacja połączenia z OBD (dostępność modułu)
        if self.connection.status() == OBDStatus.OBD_CONNECTED:
            self.logger.logEvent("[OBD] %s " % "OBD connected")
        else:
            self.logger.logEvent("[OBD] %s " % "OBD not connected")

        # weryfikacja finalnego połączenia i negocjacji protokołu z pojazdem
        if self.connection.status() == OBDStatus.CAR_CONNECTED:
            self.logger.logEvent("[OBD] %s " % "Car connected")
            self.logger.logEvent("[OBD] %s " % self.connection.protocol_name())
            return True
        else:
            self.logger.logEvent("[OBD] %s " % "Car not connected")
            return False

    def connect(self):
        # (4b) zmienna przechowująca stan połączenia z modułem OBD
        # w poniższej pętli wykonywane jest ponowne nawiązywanie połączenia z modułem ODB2
        self.statusConnected = False
        while not self.statusConnected:
            self.logger.logEvent("Connecting to OBD...")
            self.connection = self.scanAndConnect()
            self.statusConnected = self.printStatus()
            if not self.statusConnected:
                time.sleep(5)

    # (1) definicja funkcji do przetwarzania komunikatów. Parametrem metody jest linia tekstu otrzymana z modułu GPS.
    def read(self):
        self.logger.logEvent("[OBD] read.")

        loadResult = self.connection.query(obd.commands.ENGINE_LOAD)
        print("%s %s"%(loadResult,self.connection.status()))
	
        if self.connection.status() != OBDStatus.CAR_CONNECTED:
            self.connect()

        if loadResult:
            self.dataThreadLock.acquire()
            self.engineLoad = loadResult.value
            self.dataThreadLock.release()
            self.logger.logEvent("[OBD] %s " % "Load read.")

        rpmResult = self.connection.query(obd.commands.RPM)
        if self.connection.status() != OBDStatus.CAR_CONNECTED:
            self.connect()

        if rpmResult:
            self.dataThreadLock.acquire()
            self.rpm = rpmResult.value
            self.dataThreadLock.release()
            self.logger.logEvent("[OBD] %s " % "RPM read.")

        speedResult = self.connection.query(obd.commands.SPEED)
        if self.connection.status() != OBDStatus.CAR_CONNECTED:
            self.connect()

        if speedResult:
            self.dataThreadLock.acquire()
            self.speed = speedResult.value
            self.dataThreadLock.release()
            self.logger.logEvent("[OBD] %s " % "Speed read.")

        throttleResult = self.connection.query(obd.commands.THROTTLE_POS)
        if self.connection.status() != OBDStatus.CAR_CONNECTED:
            self.connect()

        if throttleResult:
            self.dataThreadLock.acquire()
            self.throttlePosition = throttleResult.value
            self.dataThreadLock.release()
            self.logger.logEvent("[OBD] %s " % "Throttle read.")

    def getLoad(self):
        return self.engineLoad

    def getRPM(self):
        return self.rpm

    def getSpeed(self):
        return self.speed

    def getThrottle(self):
        return self.throttlePosition
