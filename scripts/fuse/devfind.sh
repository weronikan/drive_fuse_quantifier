#!/bin/bash

for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev)
do
    syspath="${sysdevpath%/dev}"
    devname="$(udevadm info -q name -p $syspath)"
    [[ "$devname" == "bus/"* ]] && continue
    eval "$(udevadm info -q property --export -p $syspath)"
    [[ -z "$ID_MODEL_ID" ]] && continue
    [[ -z "$ID_VENDOR_ID" ]] && continue
    echo "/dev/$devname vendor:$ID_VENDOR_ID model:$ID_MODEL_ID serial:$ID_SERIAL_SHORT"
done
