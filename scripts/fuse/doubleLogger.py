import time


class DLogger:
    timestamp = None
    eventLogFile = None
    dataLogFile = None

    def __init__(self):
        self.timestamp = None
        self.eventLogFile = None
        self.dataLogFile = None

        self.timestamp = time.time()

        eventLogFilename = "drive_log_" + str(self.timestamp) + ".log"
        dataLogFilename = "drive_data_" + str(self.timestamp) + ".log"

        self.eventLogFile = open(eventLogFilename, "w+")
        self.dataLogFile = open(dataLogFilename, "w+")

        self.dataLogFile.write("sys_timestamp,acc_x,acc_y,acc_z,gyr_x,gyr_y,gyr_z,lat,lon,alt,gps_speed,gps_time,engine_load,rpm,obd_speed,throttle_pos\n")

    def logEvent(self, content):
        print("%s" % (content))
        self.eventLogFile.write("%s \n" % (content))

    def logData(self, contentdata):
        print("[%s] %s" % (str(time.time()), contentdata))
        self.dataLogFile.write("%s\n" % (contentdata))
