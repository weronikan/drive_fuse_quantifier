import time

import pynmea2 as pynmea2
import serial

class SensorGPS:
    dataThreadLock = None
    serialPort = None
    lat = 0.0
    lon = 0.0
    currentSpeed = 0.0
    alt = 0.0
    gpsTime = None

    def __init__(self, logger, dataLock, port):
        self.dataThreadLock = dataLock
        self.logger = logger
        self.lat = 0.0
        self.lon = 0.0
        self.currentSpeed = 0.0
        self.alt = 0.0
        self.gpsTime = None

        self.serialPort = serial.Serial(port, baudrate=9600, timeout=0.5)

    def read(self):
        self.logger.logEvent("[GPS] read.")
        line = self.serialPort.readline()

        line = line.decode('utf-8')

        print("%s\n" % (line))

        try:
            if line.find('GGA') > 0:
                result = pynmea2.parse(line)
                self.dataThreadLock.acquire()
                self.lat = result.latitude
                self.lon = result.longitude
                self.alt = result.altitude
                self.gpsTime = result.timestamp
                self.dataThreadLock.release()
                self.logger.logEvent("[GPS] %s,%s,%s,%s,%s" % (
                time.time(), result.timestamp, result.latitude, result.longitude, result.altitude))

            if line.find('GPVTG') > 0:
                result = pynmea2.parse(line)
                self.dataThreadLock.acquire()
                self.currentSpeed = result.spd_over_grnd_kmph
                self.dataThreadLock.release()
                self.logger.logEvent("[GPS] %s,%s" % (time.time(), result.spd_over_grnd_kmph))
        except Exception as err:
            self.logger.logEvent("Error: %s." % err)

    def getLat(self):
        return self.lat

    def getLon(self):
        return self.lon

    def getSpeed(self):
        return self.currentSpeed

    def getAlt(self):
        return self.alt

    def getGpsTime(self):
        return self.gpsTime
