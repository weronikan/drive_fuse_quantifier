import sys, getopt, math, time, threading
sys.path.append('/root/RTIMULib2/Linux/python/tests/')

from doubleLogger import DLogger

logger = DLogger()
dataLock = threading.Lock()

from gps import SensorGPS
from imu import SensorIMU
from obdwrapper import SensorOBD


def startOBDThread(interfaceWrapper):
    logger.logEvent("Starting separate thread for ODB.")

    try:
        obdThread = threading.Thread(target=runReadOBD, args=[interfaceWrapper])
        obdThread.start()
        return obdThread
    except:
        logger.logEvent("Error: unable to start thread OBD.")
        sys.exit(1)


def runReadOBD(obdint):
    logger.logEvent("Running separate thread for ODB.")
    while True:
        obdint.read()
        time.sleep(0.01)


def startGPSThread(gpsWrapper):
    logger.logEvent("Starting separate thread for GPS.")

    try:
        gpsThread = threading.Thread(target=runReadGPS, args=[gpsWrapper])
        gpsThread.start()
        return gpsThread
    except:
        logger.logEvent("Error: unable to start GPS thread. ")
        sys.exit(1)


def runReadGPS(gpsDev):
    logger.logEvent("Running separate thread for GPS.")
    while True:
        gpsDev.read()
        time.sleep(0.01)


# sys_timestamp,acc_x,acc_y,acc_z,gyr_x,gyr_y,gyr_z,lat,lon,alt,gps_speed,gps_time,engine_load,rpm,obd_speed,throttle_pos
# %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s
if __name__ == '__main__':
    logger.logEvent("Number of args %s"%len(sys.argv))
    if len(sys.argv) !=3:
        logger.logEvent("No arguments")
        sys.exit(2)

    logger.logEvent("OBD interface %s"%sys.argv[0])
    logger.logEvent("GPS interface %s"%sys.argv[1])

    obdInterface = SensorOBD(logger, dataLock, sys.argv[1])
    obdInterface.connect()

    imuSensor = SensorIMU(logger, dataLock)
    #imuSensor.enable()

    gpsSensor = SensorGPS(logger, dataLock, sys.argv[2])

    startGPSThread(gpsSensor)
    startOBDThread(obdInterface)

    curTime = time.time()
    sleepIntervalms = imuSensor.getInterval() / 1000.0
    while True:
        imuSensor.read()

        dataLock.acquire()
        combined = ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
            time.time(),
            imuSensor.getAccel()[0], imuSensor.getAccel()[1], imuSensor.getAccel()[2],
            imuSensor.getGyro()[0], imuSensor.getGyro()[1], imuSensor.getGyro()[2],
            gpsSensor.getLat(), gpsSensor.getLon(), gpsSensor.getAlt(), gpsSensor.getSpeed(),
            obdInterface.getLoad(), obdInterface.getRPM(), obdInterface.getSpeed(), obdInterface.getThrottle()))
        dataLock.release()

        logger.logData(combined)
        time.sleep(sleepIntervalms)
