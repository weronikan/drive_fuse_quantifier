#!/bin/bash
timestamp=$(date +%s)

echo "$date start $timestamp" >> /root/allinone/logs.log

while :
do
	echo "restarting" >> /root/allinone/logs.log
	echo "Starting with obd $1 and gps $2"
	sleep 1
	python3 main.py $1 $2
done
