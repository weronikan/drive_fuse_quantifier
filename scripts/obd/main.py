import obd                  # import biblioteki obd (ze wsparciem komunikacji OBD2)
from obd import OBDStatus   # (j.w.)
import time                 # dołączenie biblioteki czasu


# (1) Funkcja sprawdza dostępne połączenia magistrali szeregowej i nawiązuje połączenie z dostępnym modułem ODB
def scanAndConnect():
    print("Scanning ports...")

    # sprawdzamy dostępne połączenia
    ports = obd.scan_serial()
    print(str(ports))

    # nawiązujemy połączenie z pierwszym dostępnym portem
    obdconnection = obd.OBD(ports[0])

    # diagnostyczne wypisy z informacją o nawiązaniu połączenia i wynegocjowanym protokołem (ISO 14230-4)
    print(obdconnection.is_connected())
    print(obdconnection.status())
    print(obdconnection.protocol_id())
    return obdconnection


# (2) Funkcja weryfikująca poprawne nawiązanie połączenia z pojazdem
def printStatus(connection):
    # błąd braku połączenia
    if connection.status() == OBDStatus.NOT_CONNECTED:
        print("Not connected")
        return False

    # weryfikacja wstępnego połączenia z modułem ELM
    if connection.status() == OBDStatus.ELM_CONNECTED:
        print("ELM connected")
    else:
        print("ELM not connected")

    # weryfikacja połączenia z OBD (dostępność modułu)
    if connection.status() == OBDStatus.OBD_CONNECTED:
        print("OBD connected")
    else:
        print("OBD not connected")

    # weryfikacja finalnego połączenia i negocjacji protokołu z pojazdem
    if connection.status() == OBDStatus.CAR_CONNECTED:
        print("Car connected")
        print(connection.protocol_name())
        return True
    else:
        print("Car not connected")
        return False


# (3) Metoda opakowująca funkcjonalność wypisu odpowiedzi otrzymanej z OBD do dziennika (pliku wyjściowego)
def requestAndWriteValue(file, connection, command):
    file.write("," + str(connection.query(command).value))


# (4) Rozpoczęcie pracy programu
if __name__ == '__main__':
    # (4a) każde nowe uruchomienie silnika spowoduje stworzenie nowego logu, którego nazwa zawiera datę rozpoczęcia zapisu
    timestamp = time.time()
    filename = "obd_"+str(timestamp)+".log"

    # otwarcie pliku z logami
    file = open(filename, "w+")

    # (4b) zmienna przechowująca stan połączenia z modułem OBD
    # w poniższej pętli wykonywane jest ponowne nawiązywanie połączenia z modułem ODB2
    status = False
    while not status:
        file.write("Connecting to OBD... \n")
        connection = scanAndConnect()
        status = printStatus(connection)
        if not status:
            time.sleep(5)

    # (4c) niekończąca się pętla odczytująca informacje o pojeździe i zapisująca zgromadzone wyniki do pliku
    while True:
        file.write(str(time.time()))
        requestAndWriteValue(file, connection, obd.commands.ENGINE_LOAD)
        requestAndWriteValue(file, connection, obd.commands.RPM)
        requestAndWriteValue(file, connection, obd.commands.SPEED)
        requestAndWriteValue(file, connection, obd.commands.THROTTLE_POS)
        file.write("\n")
        file.flush()

