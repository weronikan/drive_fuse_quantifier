#!/bin/bash

timestamp=$(date +%s)
log_name="$1/log_obd_data_$timestamp.log"

echo "$(date) starting OBD log with $timestamp" >> "$1/startups.log"

while :
do
	echo "Created file in $log_name" >> "$1/startups.log"
	sleep 15
	python main.py >> "$log_name"
done
