import sys, getopt                              # dołączenie biblioteki systemowej

sys.path.append('.')
import RTIMU                                    # import biblioteki RTIMU do obsługi układu imu
import os.path                                  # dołączenie bibliotek wsparcia czasu i funkcji matematycznych
import math
import time

SETTINGS_FILE = "RTIMULib"                      # wskazanie pliku z parametrami kalibrującymi (plik konfiguracyjny)


print("Using settings file " + SETTINGS_FILE + ".ini")
if not os.path.exists(SETTINGS_FILE + ".ini"):  # wypis diagnostyczny
    print("Settings file does not exist, will be created")

s = RTIMU.Settings(SETTINGS_FILE)               # załadowanie pliku konfiguracyjnego
imu = RTIMU.RTIMU(s)                            # kalibracja urządzenia z użyciem wcześniej załadowanego pliku konfig.

print("IMU Name: " + imu.IMUName())             # wypis diagnostyczny

if not imu.IMUInit():                           # wypis diagnostyczny - weryfikacja nawiązania połączenia
    print("IMU Init Failed")
    sys.exit(1)
else:
    print("IMU Init Succeeded")

imu.setSlerpPower(0.02)                         # parametryzacja urządzenia. Zgodnie z treścią biblioteki uruchamiamy
imu.setGyroEnable(True)                         # żyroskop, kompas i akcelerometr na potrzeby naszych testów.
imu.setAccelEnable(True)
imu.setCompassEnable(True)

poll_interval = imu.IMUGetPollInterval()        # wypis diagnostyczny
print("Recommended Poll Interval: %dmS\n" % poll_interval)

while True:                                     # (1) pętla właściwa programu
    if imu.IMURead():
        data = imu.getIMUData()                 # pobranie danych o IMU
        accelVal = imu.getAccel()               # pobranie danych z akcelerometru
        gyroVal = imu.getGyro()                 # pobranie danych z żyroskopu
        print(str(time.time()) + ";" + str(accelVal[0]) + ";" + str(accelVal[1]) + ";" + str(accelVal[2]) + ";" + str(
            gyroVal[0]) + ";" + str(gyroVal[1]) + ";" + str(gyroVal[2]))
                                                # (1b) wypisanie zebranych wartości w postaci logu

