#!/bin/bash
timestamp=$(date +%s)
echo "$(date) starting GPS log with $timestamp" >> "$1/startups.log"

while :
do
	echo "Created file in $1/log_gps_data_$timestamp.log" >> "$1/startups.log"
	sleep 15
	python main.py >> "$1/log_gps_data_$timestamp.log"
done
