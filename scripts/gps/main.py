import serial       # import biblioteki do obsługi portu szeregowego
import pynmea2      # biblioteka do przetwarzania komunikatów NMEA - standardu GPS
import time         # załączenie biblioteki z funkcjami czasu (pobieranie i wypisywanie czasu)

# definicja zmiennej, która przechowuje nazwę portu seryjnego modułu GPS
port = "/dev/ttyUSB0"

# (1) definicja funkcji do przetwarzania komunikatów. Parametrem metody jest linia tekstu otrzymana z modułu GPS.
def parseGPS(gpsCommand):
    # (1a) Przetwarzanie komendy GGA - położenie
    if gpsCommand.find('GGA') > 0:
        result = pynmea2.parse(gpsCommand)
        print"%s,%s,%s,%s,%s" % (time.time(), result.timestamp, result.latitude, result.longitude, result.altitude)
    # (1b) Przetwarzanie komendy VTG - prędkość
    if gpsCommand.find('GPVTG') > 0:
        result = pynmea2.parse(gpsCommand)
        print"%s,%s" % (time.time(), result.spd_over_grnd_kmph)
 

# (2) nawiązanie połączenia z portem szeregowym
serialPort = serial.Serial(port, baudrate = 9600, timeout = 0.5)

# (3) kod właściwy programu
while True:
    line = serialPort.readline()
    parseGPS(line)
