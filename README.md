# Car Sensor Data Quantifier
Projekt realizowany na potrzeby pracy magisterskiej, realizowanej na Politechnice Gdańskiej. Celem badań jest stworzenie prototypowego rozwiązania, które pozwoli zgromadzić dane z sensorów podczas jazdy, a następnie dokona oceny stylu jazdy kierowcy. Celem projektu jest zarówno stworzenie urządzenia pomiarowego, opracowanie sposobu jego montażu, ale również wyznaczenie formuły kwantyfikującej styl jazdy kierowcy.

## Technologie
Do uruchomienia wymagane jest przygotowanie środowiska (maszyna z systemem linux i sensoryką):

* Python 3.6
* Linux (użyty Linaro, Asus TinkerBoard)
* [Biblioteka obsługi układu inercyjnego](https://github.com/RTIMULib/RTIMULib2)
* Biblioteka [pynmea2](https://github.com/Knio/pynmea2)
* Biblioteka [python-obd](https://python-obd.readthedocs.io/en/latest/)

## Treść projektu
Treścią tego projektu są rozwiązania tworzące dziennik zdarzeń z logami, których treść zawiera wartość próbek pomiarowych.

* /scripts/imu/ - zawiera skrypty do obsługi logowania danych z układu IMU,
* /scripts/gps/ - treścią folderu są skrypty logujące pozycje GPS,
* /scripts/obd/ - zawiera skrypty odczytujące dane z interfejsu OBD2 w pojeździe
* /tools/ - narzędzia pomocnicze

## Co zostało wykonane
Funkcjonalności:

* automatyczne uruchamianie
* logowanie danych
* synchronizacja czasowa logów
* wyznaczanie "wykroczeń"
* narzędzie do generowania wykresów sił oddziałujących na pojazd


## Jak uruchomić
Do uruchomienia, należy wystartować wybrany skrypt run/start w katalogu sensora. 

```
git clone https://weronikan@bitbucket.org/weronikan/drive_fuse_quantifier.git
cd drive_fuse_quantifier/scripts/imu/
./start.sh 			# will start in background (screen application is required)
./main.py			# will run in this console
```

## Autorzy
Weronika Narloch,
Politechnika Gdańska, 2020
 
