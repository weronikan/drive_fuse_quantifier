clear
close all
[fileName, pathName] = uigetfile('*.log','Select file');		#wskazanie pliku z danymi
fid = fopen (strcat(pathName, fileName));
systemCommand = strcat("wc -l < ", strcat(pathName, fileName), "| cut -d' ' -f1")
[status, cmdout]= system(systemCommand);
lineCount = 0;
if(status~=1)
    scanCell = textscan(cmdout,'%u %s');
    lineCount = scanCell{1}; 
else
    fprintf(1,'Failed to find line count of %s\n', fileName);
    lineCount = -1;
end

fprintf("Total number of lines is %d... \n", lineCount);

i = 1;
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);

skipSeconds = 400;		#ustalenie pominiÄ™cia pierwszych x sekund pomiaru 
finishAt = 420;			#zakoĹ„czenie sczytywania wynikĂłw pomiarĂłw w sekundzie y
firstFrame = -1;
timediff = 0;
while (true)
    logLine = fgetl(fid);
    if logLine == -1
        break
    end
    
    logLine( logLine == ',' ) = '.';
    logLine( logLine == ';' ) = ',';
    logLine( logLine == 'n' ) = '0';
    logLine( logLine == 'a' ) = '0';
    logLine( logLine == 'O' ) = '0';
    logLine( logLine == 'K' ) = '0';
    vectorLine = str2num(logLine);
    
    lengthVectorLine = length(vectorLine);
    if lengthVectorLine < 7  
      fprintf("Breaking, line too short: %d \n %s \n\n\n", lengthVectorLine, logLine)
      break
    end
    
    if i == 1
      time(1) = 0;
      dt = 0;
      prev_timestamp = vectorLine(1);
      firstFrame = prev_timestamp;
    else
      timestamp = vectorLine(1);
      if timediff > finishAt
        break;
        #continue
      end
      time(i) = time(i-1) + (timestamp - prev_timestamp); #wyznaczenie godziny pomiaru
      prev_timestamp = timestamp;
      timediff = timestamp - firstFrame;		#wyznaczenie rĂłĹĽnicy czasu, sprawdzenie czasu
      
      if timediff < skipSeconds
        #fprintf('Skipping, too early %d \n', timediff)		#jeĹ›li dany pomiar jest wczeĹ›niej niĹĽ x sekunda - pomiĹ„
        continue
      end
    end
    
    
    
    ACCEL_X(i) = vectorLine(2);		#akcelerometr i ĹĽyroskop, sczytanie odpowiednich linii
    ACCEL_Y(i) = vectorLine(3);
    ACCEL_Z(i) = vectorLine(4);
    GYRO_X(i) = vectorLine(5);
    GYRO_Y(i) = vectorLine(6);
    GYRO_Z(i) = vectorLine(7);
    
    #G_FORCE(i) = (ACCEL_X(i)^2 + ACCEL_Y(i)^2+ACCEL_Z(i)^2)^.5;
    
    i = i + 1;
    if mod(i, 1000) == 0
        fprintf('Did %d lines... \n', i);
    end
end

figure(1)		#wykreĹ›lenie osi X akcelerometru w funkcji czasu
subplot(311)
plot(time,ACCEL_X)
grid on
xlabel('time [s]')
ylabel('X axis (Turn left[-] and right[+]) [g]')

subplot(312)		#wykreĹ›lenie osi Y akcelerometru w funkcji czasu
plot(time,ACCEL_Y)
grid on
xlabel('time [s]')
ylabel('Y axis (Acceleration and Deacceleration) [g]')

subplot(313)
plot(time,ACCEL_Z)	#wykreĹ›lenie osy Z akcelerometru w funkcji czasu
grid on
xlabel('time [s]')
ylabel('Z axis (Sudden fall, bump) [g]')

figure(2)
subplot(311)
plot(time,GYRO_X)	#wykreĹ›lenie osi X ĹĽyroskopu w funkcji czasu
grid on
xlabel('time [s]')
ylabel('X GYRO Axis [deg]')

subplot(312)
plot(time,GYRO_Y)	#wykreĹ›lenie osi Y ĹĽyroskopu w funkcji czasu
grid on
xlabel('time [s]')
ylabel('Y GYRO Axis [deg]')

subplot(313)
plot(time,GYRO_Z)	#wykreĹ›lenie osi Z ĹĽyroskopu w funkcji czasu
grid on
xlabel('time [s]')
ylabel('Z GYRO Axis [deg]')


#figure(3)
#subplot(111)
#plot(time,G_FORCE)
#grid on
#xlabel('time [s]')
#ylabel('G FORCE')
