clear
close all
[fileName, pathName] = uigetfile('*.log','Select file');		#wskazanie pliku z danymi
fid = fopen (strcat(pathName, fileName));
systemCommand = strcat("wc -l < ", strcat(pathName, fileName), "| cut -d' ' -f1")
[status, cmdout]= system(systemCommand);
lineCount = 0;
if(status~=1)
    scanCell = textscan(cmdout,'%u %s');
    lineCount = scanCell{1}; 
else
    fprintf(1,'Failed to find line count of %s\n', fileName);
    lineCount = -1;
end

fprintf("Total number of lines is %d... \n", lineCount);

i = 1;
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);
logLine = fgetl(fid);

skipSeconds = 400;		#ustalenie pominięcia pierwszych x sekund pomiaru 
finishAt = 420;			#zakończenie sczytywania wyników pomiarów w sekundzie y
firstFrame = -1;
timediff = 0;
while (true)
    logLine = fgetl(fid);
    if logLine == -1
        break
    end
    
    logLine( logLine == ',' ) = '.';
    logLine( logLine == ';' ) = ',';
    logLine( logLine == 'n' ) = '0';
    logLine( logLine == 'a' ) = '0';
    logLine( logLine == 'O' ) = '0';
    logLine( logLine == 'K' ) = '0';
    vectorLine = str2num(logLine);
    
    lengthVectorLine = length(vectorLine);
    if lengthVectorLine < 16
      fprintf("Breaking, line too short: %d \n %s \n\n\n", lengthVectorLine, logLine)
      break
    end
    
    if i == 1
      time(1) = 0;
      dt = 0;
      prev_timestamp = vectorLine(1);
      firstFrame = prev_timestamp;
    else
      timestamp = vectorLine(1);
      if timediff > finishAt
        break;
        #continue
      end
      time(i) = time(i-1) + (timestamp - prev_timestamp); #wyznaczenie godziny pomiaru
      prev_timestamp = timestamp;
      timediff = timestamp - firstFrame;		#wyznaczenie różnicy czasu, sprawdzenie czasu
      
      if timediff < skipSeconds
        #fprintf('Skipping, too early %d \n', timediff)		#jeśli dany pomiar jest wcześniej niż x sekunda - pomiń
        continue
      end
    end

    # sys_timestamp,acc_x,acc_y,acc_z,gyr_x,gyr_y,gyr_z,lat,lon,alt,gps_speed,gps_time,engine_load,rpm,obd_speed,throttle_pos
    acc_x(i) = vectorLine(2);
    acc_y(i) = vectorLine(3);
    acc_z(i) = vectorLine(4);
    gyr_x(i) = vectorLine(5);
    gyr_y(i) = vectorLine(6);
    gyr_z(i) = vectorLine(7);
    lat(i) = vectorLine(8);
    lon(i) = vectorLine(9);
    alt(i) = vectorLine(10);
    gps_speed(i) = vectorLine(11);
    gps_time(i) = vectorLine(12);
    engine_load(i) = vectorLine(13);
    rpm(i) = vectorLine(14);
    obd_speed(i) = vectorLine(15);
    throttle_pos(i) = vectorLine(16);

    i = i + 1;
    if mod(i, 1000) == 0
        fprintf('Did %d lines... \n', i);
    end
end

figure(1)		
subplot(311)
plot(time,acc_y)
grid on
xlabel('time [s]')
ylabel('Y axis (Turn left[-] and right[+]) [g]')

subplot(312)
plot(time,obd_speed)
grid on
xlabel('time [s]')
ylabel('OBD speed value')

subplot(313)
plot(time,throttle_pos)	
grid on
xlabel('time [s]')
ylabel('Throttle position [%]')
